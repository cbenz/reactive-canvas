export interface ICell {
    text: string
    x: number
    y: number
}

export function createCellAtOrigin(text: string): ICell {
    return { text, x: 0, y: 0 }
}
